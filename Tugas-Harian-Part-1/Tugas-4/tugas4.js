//soal 1
console.log("LOOPING PERTAMA");
var i = 1;
while (i <= 20) {
    if (i % 2 === 0) {
        console.log(i + " - I love coding");
    }
    i++;
}
console.log("LOOPING KEDUA");
var j = 20;
while (j > 0) {
    if (j % 2 === 0) {
        console.log(j + " - I will become a frontend developer");
    }
    j--;
}

//soal 2
console.log("");
for (var index = 1; index <= 20; index++) {
    if (index % 3 === 0 && index % 2 === 1) {
        console.log(index + " - I Love Coding ");
    } else if (index % 2 === 0) {
        console.log(index + " - Berkualitas");
    } else if (index % 2 === 1) {
        console.log(index + " - Santai");
    }

}

//soal 3
hasil = "";
for (var k = 0; k <= 7; k++) {
    for (var l = 0; l < k; l++) {
        hasil += "#";
    }
    hasil += "\n";
}
console.log(hasil);

//soal 4
var kalimat = ["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"];
var hapusPertama = kalimat.shift();
var hapusDuplikasi = kalimat.splice(2, 1);
console.log(kalimat.join(" "));

console.log("")

//soal 5
var sayuran = [];

sayuran.push("Kangkung");
sayuran.push("Bayam");
sayuran.push("Buncis");
sayuran.push("Kubis");
sayuran.push("Timun");
sayuran.push("Seledri");
sayuran.push("Tauge");

sayuran.sort();

for (var m = 0; m < sayuran.length; m++) {
    console.log(m + 1 + ". " + sayuran[m])
}