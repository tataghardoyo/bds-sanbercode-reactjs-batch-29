//soal 1
console.log("-----soal 1-----");

function luasPersegiPanjang(p, l) {
    return p * l
}
function kelilingPersegiPanjang(p, l) {
    return 2 * p + 2 * l
}
function volumeBalok(p, l, t) {
    return p * l * t
}
var panjang = 12
var lebar = 4
var tinggi = 8

var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(luasPersegiPanjang)
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

//soal 2
console.log("-----soal 2-----");

function introduce(nama, umur, alamat, hobi) {
    return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!"
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!"

console.log("")

//soal 3
console.log("-----SOAL 3-----");

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992]
var objekDaftarPeserta = {}
// meng-assign key:value dari object car2
objekDaftarPeserta.nama = arrayDaftarPeserta[0]
objekDaftarPeserta.jenisKelamin = arrayDaftarPeserta[1]
objekDaftarPeserta.hobi = arrayDaftarPeserta[2]
objekDaftarPeserta.tahunLahir = arrayDaftarPeserta[3]

console.log(objekDaftarPeserta)

console.log("")

//soal 4
console.log("-----SOAL 4-----");
var buah = [{
    nama: "Nanas",
    warna: "Kuning",
    adaBijinya: false,
    harga: 9000,
},
{
    nama: "Jeruk",
    warna: "Oranye",
    adaBijinya: true,
    harga: 8000,
},
{
    nama: "Semangka",
    warna: ["Hijau", "Merah"],
    adaBijinya: true,
    harga: 10000,
},
{
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: false,
    harga: 5000,
}]
console.log(buah.filter(function (item) {
    return item.adaBijinya === false
}))

console.log("")

//soal 5
console.log("-----SOAL 5-----");
function tambahDataFilm(name, duration, gen, year) {
    var objek = {}
    objek.nama = name
    objek.durasi = duration
    objek.genre = gen
    objek.tahun = year
    return objek
}

var dataFilm = []

dataFilm.push(tambahDataFilm("LOTR", "2 jam", "action", "1999"))
dataFilm.push(tambahDataFilm("avenger", "2 jam", "action", "2019"))
dataFilm.push(tambahDataFilm("spiderman", "2 jam", "action", "2004"))
dataFilm.push(tambahDataFilm("juon", "2 jam", "horror", "2004"))

console.log(dataFilm)