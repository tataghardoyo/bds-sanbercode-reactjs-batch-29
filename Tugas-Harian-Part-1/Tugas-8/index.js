var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

function execute(waktu , tbh) {
    if (tbh < 4) {
        readBooks(waktu, books[tbh], function (baru) {
            execute(baru,tbh)
        })
    }
    tbh++
}

execute(10000,0)

