var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function execute(waktu, tbh) {
    if (tbh < 4) {
        readBooksPromise(waktu, books[tbh]).then(function (baru) {
            execute(baru, tbh)
        }).catch(function (e) { return e.message })
    }
    tbh++
}

execute(10000, 0)