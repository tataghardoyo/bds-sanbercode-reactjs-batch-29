var filterBooksPromise = require('./promise2.js')

function cetak0() {
    filterBooksPromise(true, 40).then((result) => {
        console.log(result)
    }).catch((e) => {
        console.log(e.message)
    })
}

async function cetak() {
    try {
        // var result0 = await filterBooksPromise(true, 40)
        // console.log(result0)
        var result1 = await filterBooksPromise(false, 250)
        console.log(result1)
        var result2 = await filterBooksPromise(true, 30)
        console.log(result2)
    } catch (e) {
        console.log(e.message)
    }

}

cetak0()
cetak()