//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

kataKedua = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);
kataKetiga = kataKetiga.substr(0,6) + kataKetiga.charAt(6).toUpperCase();
kataKeempat = kataKeempat.toLocaleUpperCase();

console.log(kataPertama.concat(" "+kataKedua.concat(" "+kataKetiga.concat(" "+kataKeempat))));

console.log("");

//soal 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga = "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang;
var luasSegitiga;

kelilingPersegiPanjang = 2 * parseInt(panjangPersegiPanjang) + 2 * parseInt(lebarPersegiPanjang);
console.log("sebuah persegi panjang dengan panjang " + panjangPersegiPanjang + " dan lebar " + lebarPersegiPanjang + " memiliki keliling: " + kelilingPersegiPanjang);

luasSegitiga = 0.5 * parseInt(alasSegitiga) * parseInt(tinggiSegitiga);
console.log("sebuah segitiga dengan alas " + alasSegitiga + " dan tinggi " + tinggiSegitiga + " memiliki luas: " + luasSegitiga);

console.log("");

//soal 3
var sentences = 'wah javascript itu keren sekali';

var firstWord = sentences.substring(0, 3);
var secondWord = sentences.substring(4, 14);
var thirdWord = sentences.substring(15, 18);
var fourthWord = sentences.substring(19, 24);
var fifthWord = sentences.substring(25, 31);

console.log('Kata Pertama: ' + firstWord);
console.log('Kata Kedua: ' + secondWord);
console.log('Kata Ketiga: ' + thirdWord);
console.log('Kata Keempat: ' + fourthWord);
console.log('Kata Kelima: ' + fifthWord);

console.log("");

//soal 4
var nilaiJohn = 80;
var nilaiDoe = 50;

if (nilaiJohn >= 80) {
    console.log("Nilai John " + nilaiJohn + " mendapat index A");
} else if (nilaiJohn >= 70 && nilaiJohn < 80) {
    console.log("Nilai John " + nilaiJohn + " mendapat index B");
} else if (nilaiJohn >= 60 && nilaiJohn < 70) {
    console.log("Nilai John " + nilaiJohn + " mendapat index C");
} else if (nilaiJohn >= 50 && nilaiJohn < 60) {
    console.log("Nilai John " + nilaiJohn + " mendapat index D");
} else if (nilaiJohn < 50) {
    console.log("Nilai John " + nilaiJohn + " mendapat index E");
}

if (nilaiDoe >= 80) {
    console.log("Nilai Doe " + nilaiDoe + " mendapat index A");
} else if (nilaiDoe >= 70 && nilaiDoe < 80) {
    console.log("Nilai Doe " + nilaiDoe + " mendapat index B");
} else if (nilaiDoe >= 60 && nilaiDoe < 70) {
    console.log("Nilai Doe " + nilaiDoe + " mendapat index C");
} else if (nilaiDoe >= 50 && nilaiDoe < 60) {
    console.log("Nilai Doe " + nilaiDoe + " mendapat index D");
} else if (nilaiDoe < 50) {
    console.log("Nilai Doe " + nilaiDoe + " mendapat index E");
}

console.log("");

//soal 5
var tanggal = 17;
var bulan = 7;
var tahun = 1998;

switch (bulan) {
    case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4: { console.log(tanggal + ' April ' + tahun); break; }
    case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
    case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9: { console.log(tanggal + ' September ' + tahun); break; }
    case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11: { console.log(tanggal + ' November ' + tahun); break; }
    case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
    default: { console.log('Bulan tidak ditemukan'); }
}