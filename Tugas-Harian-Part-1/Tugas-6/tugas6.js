//soal 1
console.log("----soal 1----")
let Luaslingkaran = (jari) => {
    return Math.round(Math.PI * (Math.pow(jari, 2)));
}

console.log(Luaslingkaran(7));

const Kelilinglingkaran = (jari = 7) => {
    return Math.round(2 * Math.PI * jari);
}

console.log(Kelilinglingkaran());

console.log("");

//soal 2
console.log("----soal 2----");

const introduce = (...rest) => {
    let [nama, umur, gender, pekerjaan] = rest;
    return gender === "Laki-Laki" ? `Pak ${nama} adalah seorang ${pekerjaan} yang berusia ${umur} tahun` : gender === "Wanita" ? `Bu ${nama} adalah seorang ${pekerjaan} yang berusia ${umur} tahun` : 'tidak terdefinisi';
}

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

console.log("");

//soal 3
console.log("----Soal 3----")

const newFunction = (firstName, lastName) => {
    return {
        firstName, lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
        }
    }
}

// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()

console.log("")

//soal 4
console.log("----Soal 4----")

let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

let { name: phoneName, brand: phoneBrand, year, colors } = phone
let [colorBronze, colorWhite, colorBlack] = colors

// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)

console.log("")

//soal 5
console.log("----Soal 5----")

let warna = ["biru", "merah", "kuning", "hijau"]

let dataBukuTambahan = {
    penulis: "john doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

buku.warnaSampul = [...buku.warnaSampul, ...warna]
let newobjek = { ...buku, ...dataBukuTambahan }
console.log(newobjek)