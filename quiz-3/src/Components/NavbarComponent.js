import React, { useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { MobileappContext } from '../Context/MobileappContext'
import logo from '../Public/img/logo.png'
const NavbarComponent = () => {
    let history = useHistory()
    const { cari, handleChangeCari } = useContext(MobileappContext)
    const handleSubmitCari = () =>{
        history.push(`/search/${cari}`)
    }
    return (
        <div className="topnav">
            <Link to="/">
                <img src={logo} width="70" alt="" />
            </Link>
            <Link to="/">Home</Link>
            <Link to="/mobile-list">Mobile List</Link>
            <Link to="/tentang">About</Link>
            <form onSubmit={handleSubmitCari}>
                <input type="text" value={cari} onChange={handleChangeCari}/>
                <input type="submit" value="Cari"/>
            </form>
        </div>
    )
}

export default NavbarComponent
