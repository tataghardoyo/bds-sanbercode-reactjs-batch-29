import React from 'react'

const FooterComponent = () => {
    return (
        <footer>
            <h5>&copy; Quiz 3 ReactJS Sanbercode</h5>
        </footer>
    )
}

export default FooterComponent
