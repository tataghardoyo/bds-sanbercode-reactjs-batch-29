import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { MobileappContext } from './Context/MobileappContext'

const MobileForm = () => {
    let { id } = useParams()

    const { name, description, category, size, price, rating, image_url,
        release_year, is_android_app, is_ios_app, handleChangeNama,
        handleChangeDescription,
        handleChangeCategory,
        handleChangeSize,
        handleChangePrice,
        handleChangeRating,
        handleChangeImage,
        handleChangeRelease,
        handleChangeIos,
        handleChangeAndroid,
        handleSubmit,
        getDataById} = useContext(MobileappContext)

    useEffect(() => {
        if (id !== undefined) {
            getDataById(id)
        } 

    }, [])


    return (
        <div className="row">
            <div className="section">
                <h1>Form Mobile App</h1>
                <form onSubmit={handleSubmit}>
                    <div>
                        <label className="label-form">Name</label>
                        <input className="inputan" type="text" value={name} onChange={handleChangeNama} required />
                    </div>
                    <div>
                        <label className="label-form">Description</label>
                        <textarea cols="30" value={description} onChange={handleChangeDescription} required />
                    </div>
                    <div>
                        <label className="label-form">Category</label>
                        <input className="inputan" type="text" value={category} onChange={handleChangeCategory} required />
                    </div>
                    <div>
                        <label className="label-form">Year</label>
                        <input type="number" value={release_year} onChange={handleChangeRelease} required />
                    </div>
                    <div>
                        <label className="label-form">Size</label>
                        <input type="number" value={size} onChange={handleChangeSize} required />
                    </div>
                    <div>
                        <label className="label-form">Price</label>
                        <input type="number" value={price} onChange={handleChangePrice} required />
                    </div>
                    <div>
                        <label className="label-form">Rating</label>
                        <input type="number" value={rating} onChange={handleChangeRating} required />
                    </div>
                    <div>
                        <label className="label-form">Image URL</label>
                        <input className="inputan" type="text" value={image_url} onChange={handleChangeImage} required />
                    </div>
                    <div>
                        <div>
                            <label><b>Platform</b></label>
                        </div>
                        <div style={{ marginLeft: "10px" }}>
                            <label className="label-form">Android</label>
                            <input type="checkbox" defaultChecked={is_android_app} onChange={handleChangeAndroid} />
                        </div>
                        <div style={{ marginLeft: "10px" }}>
                            <label className="label-form">iOS</label>
                            <input type="checkbox" defaultChecked={is_ios_app} onChange={handleChangeIos} />
                        </div>
                    </div>
                    <input className="inputansubmit" type="submit" value="Submit" />
                </form>
            </div>
        </div>
    )
}

export default MobileForm
