import Routes from "./Routes/Routes";
import './Public/css/style.css'

function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
