import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import About from '../About'
import FooterComponent from '../Components/FooterComponent'
import NavbarComponent from '../Components/NavbarComponent'
import { MobileappProvider } from '../Context/MobileappContext'
import Home from '../Home'
import MobileForm from '../MobileForm'
import MobileList from '../MobileList'
import MobileSearch from '../MobileSearch'

const Routes = () => {
    return (
        <>
            <Router>
                <MobileappProvider>
                    <NavbarComponent />
                    <Switch>
                        <Route path="/" exact>
                            <Home />
                        </Route>
                        <Route path="/mobile-list" exact>
                            <MobileList />
                        </Route>
                        <Route path="/tentang" exact>
                            <About />
                        </Route>
                        <Route path="/mobile-form" exact >
                            <MobileForm />
                        </Route>
                        <Route path="/mobile-form/edit/:id" exact>
                            <MobileForm />
                        </Route>
                        <Route path="/search/:valueOfSearch" exact>
                            <MobileSearch />
                        </Route>
                    </Switch>
                    <FooterComponent />
                </MobileappProvider>
            </Router>
        </>
    )
}

export default Routes
