import React, { createContext, useState, useEffect } from 'react'
import axios from "axios"
import { useHistory } from 'react-router-dom'

export const MobileappContext = createContext()

export const MobileappProvider = (props) => {
    let history = useHistory()
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [category, setCategory] = useState("")
    const [size, setSize] = useState("")
    const [price, setPrice] = useState("")
    const [rating, setRating] = useState("")
    const [image_url, setImage] = useState("")
    const [release_year, setRelease] = useState(2007)
    const [is_android_app, setAndroid] = useState(true)
    const [is_ios_app, setIos] = useState(true)

    const handleChangeNama = (event) => {
        setName(event.target.value);
    }

    const handleChangeDescription = (event) => {
        setDescription(event.target.value);
    }

    const handleChangeCategory = (event) => {
        setCategory(event.target.value);
    }

    const handleChangeSize = (event) => {
        setSize(event.target.value);
    }

    const handleChangePrice = (event) => {
        setPrice(event.target.value);
    }

    const handleChangeRating = (event) => {
        setRating(event.target.value);
    }

    const handleChangeImage = (event) => {
        setImage(event.target.value);
    }

    const handleChangeRelease = (event) => {
        setRelease(event.target.value);
    }

    const handleChangeIos = (event) => {
        setIos(event.target.value);
    }

    const handleChangeAndroid = (event) => {
        setAndroid(event.target.value);
    }


    const [cari, setCari] = useState("")

    const handleChangeCari = (event) => {
        setCari(event.target.value);
    }
    

    const [currentId, setCurrentId] = useState(null)

    const [dataApp, setDataApp] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
            setDataApp(result.data.map(x => {
                return {
                    id: x.id, name: x.name, description: x.description, category: x.category,
                    size: x.size, price: x.price, rating: x.rating, image_url: x.image_url, release_year: x.release_year,
                    is_android_app: x.is_android_app, is_ios_app: x.is_ios_app,
                }
            }))

        }
        fetchData()
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()
        if (release_year >= 2007 && release_year <= 2021) {
            if (currentId === null) {
                // untuk create data baru
                axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
                    name: name, description: description, category: category,
                    size: size, price: price, rating: rating, image_url: image_url, release_year: release_year,
                    is_android_app: is_android_app, is_ios_app: is_ios_app
                })
                    .then(res => {
                        let data = res.data
                        setDataApp([...dataApp, {
                            id: data.id, name: data.name, description: data.description, category: data.category,
                            size: data.size, price: data.price, rating: data.rating, image_url: data.image_url,
                            release_year: data.release_year, is_android_app: data.is_android_app,
                            is_ios_app: data.is_ios_app
                        }])
                        alert('Data berhasil ditambah!')
                    })
            } else {
                axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`, {
                    name: name, description: description, category: category,
                    size: size, price: price, rating: rating, image_url: image_url, release_year: release_year,
                    is_android_app: is_android_app, is_ios_app: is_ios_app
                })
                    .then(() => {
                        let singleApp = dataApp.find(el => el.id === currentId)
                        singleApp.name = name
                        singleApp.description = description
                        singleApp.category = category
                        singleApp.size = size
                        singleApp.price = price
                        singleApp.rating = rating
                        singleApp.image_url = image_url
                        singleApp.release_year = release_year
                        singleApp.is_android_app = is_android_app
                        singleApp.is_ios_app = is_ios_app

                        setDataApp([...dataApp])
                        alert('Data berhasil diedit!');
                    })
            }
        } else {
            alert('Data release year 2007 - 2021!')
        }
        setName("")
        setDescription("")
        setCategory("")

        setSize("")
        setPrice("")
        setRating("")

        setImage("")
        setRelease("")
        setAndroid(true)
        setIos(true)

        setCurrentId(null)
        history.push("/mobile-list")
    }

    const getDataById = (id) => {
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
            .then(res => {
                let data = res.data
                setName(data.name)
                setDescription(data.description)
                setCategory(data.category)

                setSize(data.size)
                setPrice(data.price)
                setRating(data.rating)

                setImage(data.image_url)
                setRelease(data.release_year)
                setAndroid(data.is_android_app)
                setIos(data.is_ios_app)

                setCurrentId(data.id)
            })
    }

    const handleEdit = (event) => {
        let idApp = event.currentTarget.value
        history.push(`/mobile-form/edit/${idApp}`)
    }

    const handleDelete = (event) => {
        let idApp = parseInt(event.currentTarget.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idApp}`)
            .then(() => {
                let newApp = dataApp.filter(el => { return el.id !== idApp })
                setDataApp(newApp)
                alert('Data Terhapus');
            })
    }

    return (
        <MobileappContext.Provider value={{
            name,
            setName,
            description,
            setDescription,
            category,
            setCategory,
            size,
            setSize,
            price,
            setPrice,
            rating,
            setRating,
            image_url,
            setImage,
            release_year,
            setRelease,
            is_android_app,
            setAndroid,
            is_ios_app,
            setIos,
            dataApp,
            setDataApp,
            handleChangeNama,
            handleChangeDescription,
            handleChangeCategory,
            handleChangeSize,
            handleChangePrice,
            handleChangeRating,
            handleChangeImage,
            handleChangeRelease,
            handleChangeIos,
            handleChangeAndroid,
            handleSubmit,
            handleEdit,
            getDataById,
            handleDelete,
            cari,
            setCari,
            handleChangeCari

        }}>
            {props.children}
        </MobileappContext.Provider>

    )
}
