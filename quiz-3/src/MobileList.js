import React, { useContext } from 'react'
import { Table, Button } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
import { MobileappContext } from './Context/MobileappContext'
const MobileList = () => {
    const { dataApp, handleEdit, handleDelete } = useContext(MobileappContext)
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
        },
        {
            title: 'Description',
            dataIndex: 'description',
        },
        {
            title: 'Category',
            dataIndex: 'category',
        },
        {
            title: 'Size',
            dataIndex: 'size',
            render: (res, index) => (
                <>
                    {
                        (index.size >= 0 && index.size <= 900) ? (index.size+" MB") :
                        (index.price >= 1000) && (index.size/1000+" GB")
                    }
                    
                </>
            ),
            sorter: {
                compare: (a, b) => a.size - b.size,
                multiple: 1,
            },
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
            render: (res, index) => (
                <>
                    {
                        index.price === 0 ? ("Free") : (index.price)
                    }
                </>
            ),
            sorter: {
                compare: (a, b) => a.size - b.size,
                multiple: 1,
            },
        },
        {
            title: 'Rating',
            dataIndex: 'rating',
            sorter: {
                compare: (a, b) => a.rating - b.rating,
                multiple: 1,
            },
        },
        {
            title: 'Image',
            dataIndex: 'image_url',
        },
        {
            title: 'Release(year)',
            dataIndex: 'release_year',
            sorter: {
                compare: (a, b) => a.release_year - b.release_year,
                multiple: 1,
            },
        },
        {
            title: 'Android App',
            dataIndex: 'is_android_app',
        },
        {
            title: 'iOS App',
            dataIndex: 'is_ios_app',
        },
        {
            title: 'Aksi',
            key: 'action',
            render: (res, index) => (
                <>
                    <Button style={{ marginRight: "10px" }} value={res.id} onClick={handleEdit}><EditOutlined /></Button>
                    <Button type="danger" value={res.id} onClick={handleDelete}><DeleteOutlined /></Button>
                </>
            )
        },
    ];

    const data = dataApp

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }
    return (
        <div className="row">
            <div className="section" style={{ zIndex: "-99999" }}>
                <h1>Movie List</h1>
                <Link to="/mobile-form"><Button type="primary">Tambah</Button></Link>
                <Table columns={columns} dataSource={data} onChange={onChange} />
            </div>
        </div>
    )
}

export default MobileList
