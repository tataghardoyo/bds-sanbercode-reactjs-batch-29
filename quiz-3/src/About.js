import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'antd'

const About = () => {
    return (
        <div className="row">
            <div className="section">
                <h1>About</h1>
                <div className="pembungkus">
                    <h3 className="judul">Data Peserta Sanbercode Bootcamp Reactjs</h3>
                    <ol>
                        <li><b>Nama: </b>Tatag Hardoyo</li>
                        <li><b>Email: </b>tataghardoyo@gmail.com</li>
                        <li><b>Sistem Operasi yang digunakan: </b>Ubuntu 21.0.0</li>
                        <li><b>Akun Gitlab: </b>@tataghardoyo</li>
                        <li><b>Akun Telegram: </b>@tataghrdy</li>
                    </ol>
                </div>
                <Link className="link" to="/"><Button type="primary">Kembali Ke Index</Button></Link>
            </div>
        </div>
    )
}

export default About
