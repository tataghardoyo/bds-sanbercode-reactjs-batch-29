import React, { useContext } from 'react'
import { MobileappContext } from './Context/MobileappContext'

const Home = () => {
    const { dataApp } = useContext(MobileappContext)
    return (
        <div className="row">
            <div className="section">
                {
                    dataApp.map((item, idx) => (
                        <div className="card" key={idx}>
                            <div>
                                <h2>{item.name}</h2>
                                <h5>Release Year : {item.release_year}</h5>
                                <img className="fakeimg" style={{ width: "50%", height: "300px", objectFit: "cover" }} src={item.image_url} alt="" />
                                <br />
                                <br />
                                <div>
                                    <strong>Price:
                                        {
                                            item.price === 0 ? ("Free") : (item.price)
                                        }
                                    </strong><br />
                                    <strong>Rating: {item.rating}</strong><br />
                                    <strong>Size:
                                        {
                                            (item.size >= 0 && item.size <= 900) ? (item.size + " MB") :
                                                (item.price >= 1000) && (item.size / 1000 + " GB")
                                        }
                                    </strong><br />
                                    <strong style={{ marginRight: "10px" }}>Platform :
                                    </strong>
                                    {item.is_android_app === 1 && ("Android")} ,
                                    {item.is_ios_app === 1 && (" iOS")}
                                    <br />
                                </div>
                                <p>
                                    <strong style={{ marginRight: "10px" }}>Description :</strong>
                                    {item.description}
                                </p>

                                <hr />
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Home
