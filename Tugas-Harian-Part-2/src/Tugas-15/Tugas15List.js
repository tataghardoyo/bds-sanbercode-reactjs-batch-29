import React, { useContext } from 'react'
import { Table, Button } from 'antd'
import { MahasiswaContext } from '../Context/MahasiswaContext'
import { Link } from "react-router-dom"

const Tugas15List = () => {
    const { nilaiSiswa, handleEdit3, handleDelete } = useContext(MahasiswaContext)

    const columns = [
        {
            title: 'Nama',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Mata Kuliah',
            dataIndex: 'course',
            key: 'course',
            sorter: {
                compare: (a, b) => a.course - b.course,
                multiple: 3,
            },
        },
        {
            title: 'Nilai',
            dataIndex: 'score',
            key: 'score',
            sorter: {
                compare: (a, b) => a.score - b.score,
                multiple: 2,
            },
        },
        {
            title: 'Index Nilai',
            dataIndex: 'indeksnilai',
            key: 'indeksnilai',
            render: (res, index) => (
                <>
                    {
                        index.score >= 80 && ("A")
                    }
                    {
                        index.score >= 70 && index.score < 80 && ("B")
                    }
                    {
                        index.score >= 60 && index.score < 70 && ("C")
                    }
                    {
                        index.score >= 50 && index.score < 60 && ("C")
                    }
                    {
                        index.score < 50 && ("E")
                    }

                </>
            )
        },
        {
            title: 'Aksi',
            key: 'action',
            render: (res, index) => (
                <>
                    <Button style={{ marginRight: "30px" }} value={res.id} onClick={handleEdit3}>Edit</Button>
                    <Button type="danger" value={res.id} onClick={handleDelete}>Hapus</Button>
                </>
            )
        },
    ];

    const data = nilaiSiswa

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }
    return (
        <div style={{ maxWidth: "60%", margin: "40px auto" }}>
            
            <h1 className="tengah">Daftar List Mahasiswa</h1>
            <Link to="/tugas15/create"><Button type="primary">Tambah</Button></Link>
            <Table columns={columns} dataSource={data} onChange={onChange} />
        </div>
    )
}

export default Tugas15List
