import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Navbar from '../Components/Nav';
import Tugas9 from '../Tugas-9/Tugas9';
import Tugas10 from '../Tugas-10/Tugas10';
import Tugas11 from '../Tugas-11/Tugas11';
import Tugas12 from '../Tugas-12/Tugas12';
import Mahasiswa from '../Tugas-13/Mahasiswa';
import StudentScore from '../Tugas-14/StudentScore';
import StudentScoreForm from '../Tugas-14/StudentScoreForm';
import { MahasiswaProvider } from '../Context/MahasiswaContext'
import { SwitchThemeProvider } from '../Context/SwitchTheme';
import TombolSwitch from '../Components/ButtonSwitch';
import Tugas15List from '../Tugas-15/Tugas15List';
import Tugas15Form from '../Tugas-15/Tugas15Form';
const Routes = () => {
    return (
        <>
            <Router>
                <MahasiswaProvider>
                    <SwitchThemeProvider>
                        <Navbar />
                        <Switch>
                            <Route path="/" exact>
                                <Tugas9 />
                            </Route>
                            <Route path="/tugas10" exact>
                                <div>
                                    <Tugas10 />
                                    <Tugas9 />
                                </div>
                            </Route>
                            <Route path="/tugas11" exact>
                                <Tugas11 />
                            </Route>
                            <Route path="/tugas12" exact>
                                <Tugas12 />
                            </Route>
                            <Route path="/tugas13" exact>
                                <Mahasiswa />
                            </Route>
                            <Route path="/tugas14" exact>
                                <TombolSwitch />
                                <StudentScore />
                            </Route>
                            <Route path="/tugas14/create" exact>
                                <StudentScoreForm />
                            </Route>
                            <Route path="/tugas14/edit/:id" exact>
                                <StudentScoreForm />
                            </Route>
                            <Route path="/tugas15" exact>
                                <Tugas15List />
                            </Route>
                            <Route path="/tugas15/create" exact>
                                <Tugas15Form />
                            </Route>
                            <Route path="/tugas15/edit/:id" exact>
                                <Tugas15Form />
                            </Route>
                        </Switch>
                    </SwitchThemeProvider>
                </MahasiswaProvider>
            </Router>
        </>
    )
}

export default Routes
