import React from 'react'

const Checklis = (props) => {
    return (
        <div className="pembungkus-ceklist">
            <input className="ceklist" type="checkbox" name="centang" />
            <label>{props.labele}</label>
            <hr className="garis-bawah" />
        </div>
    )
}

export default Checklis
