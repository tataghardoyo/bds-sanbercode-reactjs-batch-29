import React, { useState, useEffect } from 'react'
import axios from "axios"
const Tugas12 = () => {
    const [nilaiSiswa, setNilaiSiswa] = useState([])

    const [inputNama, setInputNama] = useState("")
    const [inputMatakul, setInputMatakul] = useState("")
    const [inputNilai, setInputNilai] = useState(0)

    const [currentId, setCurrentId] = useState(null)

    const handleChangeNama = (event) => {
        setInputNama(event.target.value);
    }

    const handleChangeMatakul = (event) => {
        setInputMatakul(event.target.value);
    }

    const handleChangeNilai = (event) => {
        setInputNilai(event.target.value);
    }
    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            setNilaiSiswa(result.data.map(x => { return { id: x.id, name: x.name, course: x.course, score: x.score } }))
        }

        fetchData()
    }, [])
    const handleEdit = (event) => {
        let idSiswa = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idSiswa}`)
            .then(res => {
                let data = res.data
                setInputNama(data.name)
                setInputMatakul(data.course)
                setInputNilai(data.score)
                setCurrentId(data.id)
            })
    }
    const handleDelete = (event) => {
        let idSiswa = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idSiswa}`)
            .then(() => {
                let newSiswa = nilaiSiswa.filter(el => { return el.id !== idSiswa })
                setNilaiSiswa(newSiswa)
            })
    }
    const handleSubmit = (event) => {
        event.preventDefault()

        if (inputNilai >= 0 && inputNilai <= 100) {
            if (currentId === null) {
                // untuk create data baru
                axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(res => {
                        let data = res.data
                        setNilaiSiswa([...nilaiSiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
                    })
            } else {
                axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(() => {
                        let singleSiswa = nilaiSiswa.find(el => el.id === currentId)
                        singleSiswa.name = inputNama
                        singleSiswa.course = inputMatakul
                        singleSiswa.score = inputNilai
                        setNilaiSiswa([...nilaiSiswa])
                    })
            }
        } else {
            alert("Masukkan Nilai antara 0-100")
        }
        setInputNama("")
        setInputMatakul("")
        setInputNilai("")
        setCurrentId(null)
    }
    return (
        <div className="main-bungkus">

            <h1 className="tengah">Daftar Nilai Mahasiswa</h1>
            {nilaiSiswa !== null &&
                <table className="pembungkus-tabel styled-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Nilai</th>
                            <th>Indeks Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            nilaiSiswa.map((val, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{val.name}</td>
                                        <td>{val.course}</td>
                                        <td>{val.score}</td>
                                        <td>
                                            {
                                                val.score >= 80 && ("A")
                                            }
                                            {
                                                val.score >= 70 && val.score < 80 && ("B")
                                            }
                                            {
                                                val.score >= 60 && val.score < 70 && ("C")
                                            }
                                            {
                                                val.score >= 50 && val.score < 60 && ("C")
                                            }
                                            {
                                                val.score < 50 && ("E")
                                            }
                                        </td>
                                        <td>
                                            <button className="tbl wrn-tbl1" onClick={handleEdit} value={val.id}>Edit</button>
                                            <button className="tbl wrn-tbl" onClick={handleDelete} value={val.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            }
            <h1 className="tengah">Form Nilai Mahasiswa</h1>
            <form onSubmit={handleSubmit} className="pembungkus-input">
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nama : </b></label>
                    <input type="text" name="nama" value={inputNama} onChange={handleChangeNama} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Mata Kuliah : </b></label>
                    <input type="text" name="matakuliah" value={inputMatakul} onChange={handleChangeMatakul} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nilai : </b></label>
                    <input type="number" name="nilai" value={inputNilai} onChange={handleChangeNilai} required />
                </div>
                <div className="tombol-input">
                    <input className="tombol-submit2" type="submit" value="submit" />
                </div>
            </form>
        </div>
    )
}

export default Tugas12
