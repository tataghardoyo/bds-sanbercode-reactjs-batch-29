import React, { useState } from 'react'

const Tugas11 = () => {
    var daftarBuah = [
        { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
        { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
        { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
        { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 }
    ]
    const [buah, setBuah] = useState(daftarBuah)

    const [inputNama, setInputNama] = useState("")
    const [inputHargaTot, setInputHargaTot] = useState(0)
    const [inputBeratTot, setInputBeratTot] = useState("")

    const [currentIndex, setCurrentIndex] = useState(-1)

    const handleChangeNama = (event) => {
        setInputNama(event.target.value);
    }

    const handleChangeHrgTot = (event) => {
        setInputHargaTot(event.target.value);
    }

    const handleChangeBrtTot = (event) => {
        setInputBeratTot(event.target.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (inputBeratTot >= 2000) {
            let newData = buah
            var tambahBuah = {}
            tambahBuah.nama = inputNama
            tambahBuah.hargaTotal = inputHargaTot
            tambahBuah.beratTotal = inputBeratTot
            if (currentIndex === -1) {
                newData = [...buah, tambahBuah]
            } else {
                newData[currentIndex] = tambahBuah
            }
            setBuah(newData)
            setInputNama("")
            setInputHargaTot("")
            setInputBeratTot("")
        } else {
            alert("Masukkan Berat Minimal 2 kg")
        }
    }
    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = buah[index]
        let newData = buah.filter((e) => { return e !== deletedItem })
        setBuah(newData)
    }

    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editValueNama = buah[index].nama
        let editValueHrgTot = buah[index].hargaTotal
        let editValueBrtTot = buah[index].beratTotal
        setInputNama(editValueNama)
        setInputHargaTot(editValueHrgTot)
        setInputBeratTot(editValueBrtTot)
        setCurrentIndex(event.target.value)

    }
    return (
        <div className="pembungkus-buah">
            <h1 className="tengah">Daftar Harga Buah</h1>
            <table className="pembungkus-tabel styled-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Harga total</th>
                        <th>Berat total</th>
                        <th>Harga per kg</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        buah.map((val, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{val.nama}</td>
                                    <td>{val.hargaTotal}</td>
                                    <td>{val.beratTotal / 1000} Kg</td>
                                    <td>{val.hargaTotal / (val.beratTotal / 1000)}</td>
                                    <td>
                                        <button className="tbl wrn-tbl1" onClick={handleEdit} value={index}>Edit</button>
                                        <button className="tbl wrn-tbl" onClick={handleDelete} value={index}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>

            <h1 className="tengah">Form Daftar Harga Buah</h1>
            <form onSubmit={handleSubmit} className="pembungkus-input">
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nama : </b></label>
                    <input type="text" name="nama" value={inputNama} onChange={handleChangeNama} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Harga Total : </b></label>
                    <input type="number" name="hargatotal" value={inputHargaTot} onChange={handleChangeHrgTot} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Berat Total(dalam gram) : </b></label>
                    <input type="number" name="berat" value={inputBeratTot} onChange={handleChangeBrtTot} required />
                </div>
                <div className="tombol-input">
                <input className="tombol-submit2" type="submit" value="submit" />
                </div>
            </form>
        </div>
    )
}

export default Tugas11
