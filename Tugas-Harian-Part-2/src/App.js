import './App.css';
import './public/css/style.css';
import Routes from './Routes/Routes';

// import Tugas9 from './Tugas-9/Tugas9';
// import Tugas10 from './Tugas-10/Tugas10';

// import Tugas11 from './Tugas-11/Tugas11';
// import Tugas12 from './Tugas-12/Tugas12';
// import Mahasiswa from './Tugas-13/Mahasiswa';

function App() {
  return (
    <div>
      {/* <Tugas10 />
      <Tugas9 /> */}
      {/* <Tugas11/> */}
      {/* <Tugas12/> */}
      {/* <Mahasiswa/> */}
      <Routes/>
    </div>
  );
}

export default App;
