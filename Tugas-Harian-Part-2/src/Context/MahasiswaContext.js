import React, { createContext, useState, useEffect } from 'react'
import axios from "axios"
import { useHistory } from 'react-router-dom'
import { message} from 'antd';
export const MahasiswaContext = createContext()

export const MahasiswaProvider = props => {
    let history = useHistory()

    const [nilaiSiswa, setNilaiSiswa] = useState([])

    const [inputNama, setInputNama] = useState("")
    const [inputMatakul, setInputMatakul] = useState("")
    const [inputNilai, setInputNilai] = useState(0)


    const [currentId, setCurrentId] = useState(null)

    const handleChangeNama = (event) => {
        setInputNama(event.target.value);
    }

    const handleChangeMatakul = (event) => {
        setInputMatakul(event.target.value);
    }

    const handleChangeNilai = (event) => {
        setInputNilai(event.target.value);
    }

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
            setNilaiSiswa(result.data.map(x => { return { id: x.id, name: x.name, course: x.course, score: x.score } }))
        }

        fetchData()
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault()

        if (inputNilai >= 0 && inputNilai <= 100) {
            if (currentId === null) {
                // untuk create data baru
                axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(res => {
                        let data = res.data
                        setNilaiSiswa([...nilaiSiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
                    })
            } else {
                axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(() => {
                        let singleSiswa = nilaiSiswa.find(el => el.id === currentId)
                        singleSiswa.name = inputNama
                        singleSiswa.course = inputMatakul
                        singleSiswa.score = inputNilai
                        setNilaiSiswa([...nilaiSiswa])
                    })
            }
        } else {
            alert("Masukkan Nilai antara 0-100")
        }
        setInputNama("")
        setInputMatakul("")
        setInputNilai("")
        setCurrentId(null)
    }

    // const [movie, setMovie] = useState([
    //     { name: "Harry Potter", lengthOfTime: 120 },
    //     { name: "Sherlock Holmes", lengthOfTime: 125 },
    //     { name: "Avengers", lengthOfTime: 130 },
    //     { name: "Spiderman", lengthOfTime: 124 },
    // ])
    // const handle = () => {
    //     alert("coba")
    // }

    const handleEdit = (event) => {
        let idSiswa = event.target.value
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idSiswa}`)
            .then(res => {
                let data = res.data
                setInputNama(data.name)
                setInputMatakul(data.course)
                setInputNilai(data.score)
                setCurrentId(data.id)
            })
    }
    const handleDelete = (event) => {
        let idSiswa = parseInt(event.currentTarget.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idSiswa}`)
            .then(() => {
                let newSiswa = nilaiSiswa.filter(el => { return el.id !== idSiswa })
                setNilaiSiswa(newSiswa)
                message.success('Data Terhapus')
            })
    }
    const handleSubmit2 = (event) => {
        event.preventDefault()

        if (inputNilai >= 0 && inputNilai <= 100) {
            if (currentId === null) {
                // untuk create data baru
                axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(res => {
                        let data = res.data
                        setNilaiSiswa([...nilaiSiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
                        message.success('Data berhasil ditambah!')
                    })
            } else {
                axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(() => {
                        let singleSiswa = nilaiSiswa.find(el => el.id === currentId)
                        singleSiswa.name = inputNama
                        singleSiswa.course = inputMatakul
                        singleSiswa.score = inputNilai
                        setNilaiSiswa([...nilaiSiswa])
                        message.success('Data berhasil diedit!');
                    })
            }
        } else {
            alert("Masukkan Nilai antara 0-100")
        }
        setInputNama("")
        setInputMatakul("")
        setInputNilai("")
        setCurrentId(null)
        history.push("/tugas14")
    }
    const handleEdit2 = (event) => {
        let idSiswa = event.currentTarget.value
        history.push(`/tugas14/edit/${idSiswa}`)
    }
    const getDataById = (id) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
            .then(res => {
                let data = res.data
                setInputNama(data.name)
                setInputMatakul(data.course)
                setInputNilai(data.score)
                setCurrentId(data.id)
            })
    }
    const handleSubmit3 = (event) => {
        event.preventDefault()

        if (inputNilai >= 0 && inputNilai <= 100) {
            if (currentId === null) {
                // untuk create data baru
                axios.post(`http://backendexample.sanbercloud.com/api/student-scores`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(res => {
                        let data = res.data
                        setNilaiSiswa([...nilaiSiswa, { id: data.id, name: data.name, course: data.course, score: data.score }])
                        message.success('Data berhasil ditambah!')
                    })
            } else {
                axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name: inputNama, course: inputMatakul, score: inputNilai })
                    .then(() => {
                        let singleSiswa = nilaiSiswa.find(el => el.id === currentId)
                        singleSiswa.name = inputNama
                        singleSiswa.course = inputMatakul
                        singleSiswa.score = inputNilai
                        setNilaiSiswa([...nilaiSiswa])
                        message.success('Data berhasil diedit!');
                    })
            }
        } else {
            alert("Masukkan Nilai antara 0-100")
        }
        setInputNama("")
        setInputMatakul("")
        setInputNilai("")
        setCurrentId(null)
        history.push("/tugas15")
    }
    const handleEdit3 = (event) => {
        let idSiswa = event.currentTarget.value
        history.push(`/tugas15/edit/${idSiswa}`)
    }
    return (
        <MahasiswaContext.Provider value={{
            // movie,
            // setMovie,
            // handle,
            nilaiSiswa,
            setNilaiSiswa,
            inputNama,
            setInputNama,
            inputMatakul,
            setInputMatakul,
            inputNilai,
            setInputNilai,
            handleChangeNama,
            handleChangeMatakul,
            handleChangeNilai,
            handleSubmit,
            handleEdit,
            handleDelete,
            handleSubmit2,
            handleEdit2,
            getDataById,
            handleSubmit3,
            handleEdit3,

        }}>
            {props.children}
        </MahasiswaContext.Provider>
    )
}

