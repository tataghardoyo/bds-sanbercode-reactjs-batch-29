import React, { createContext, useState } from 'react'

export const SwitchThemeContext = createContext()

export const SwitchThemeProvider = (props) => {
    const [tema, setTema] = useState("dark")

    return (
        <SwitchThemeContext.Provider value={{
            tema,
            setTema,
        }}>
            {props.children}
        </SwitchThemeContext.Provider>
    )
}
