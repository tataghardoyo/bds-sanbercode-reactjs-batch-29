import React, { useContext } from "react"
import { MahasiswaContext } from "../Context/MahasiswaContext"

const ListMahasiswa = () => {
    const { nilaiSiswa, handleEdit, handleDelete } = useContext(MahasiswaContext)
    return (
        <div>
            <h1 className="tengah">Daftar Nilai Mahasiswa</h1>
            {nilaiSiswa !== null &&
                <table className="pembungkus-tabel styled-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Mata Kuliah</th>
                            <th>Nilai</th>
                            <th>Indeks Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            nilaiSiswa.map((val, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{val.name}</td>
                                        <td>{val.course}</td>
                                        <td>{val.score}</td>
                                        <td>
                                            {
                                                val.score >= 80 && ("A")
                                            }
                                            {
                                                val.score >= 70 && val.score < 80 && ("B")
                                            }
                                            {
                                                val.score >= 60 && val.score < 70 && ("C")
                                            }
                                            {
                                                val.score >= 50 && val.score < 60 && ("C")
                                            }
                                            {
                                                val.score < 50 && ("E")
                                            }
                                        </td>
                                        <td>
                                            <button className="tbl wrn-tbl1" onClick={handleEdit} value={val.id}>Edit</button>
                                            <button className="tbl wrn-tbl" onClick={handleDelete} value={val.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            }
        </div>
    )
}

export default ListMahasiswa
