import React from 'react'
// import { MahasiswaProvider } from '../Context/MahasiswaContext'
import FormMahasiswa from './FormMahasiswa'
import ListMahasiswa from './ListMahasiswa'

const Mahasiswa = () => {
    return (
        <div className="main-bungkus">
            {/* <MahasiswaProvider> */}
                <ListMahasiswa />
                <FormMahasiswa />
            {/* </MahasiswaProvider> */}
        </div>
    )
}

export default Mahasiswa
