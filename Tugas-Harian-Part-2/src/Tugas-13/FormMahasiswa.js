import React, { useContext } from "react"
import { MahasiswaContext } from "../Context/MahasiswaContext"

const FormMahasiswa = () => {
    const { inputNama, inputMatakul, inputNilai, handleChangeNama, handleChangeMatakul, handleChangeNilai, handleSubmit } = useContext(MahasiswaContext)
    return (
        <>
            <h1 className="tengah">Form Nilai Mahasiswa</h1>
            <form onSubmit={handleSubmit} className="pembungkus-input">
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nama : </b></label>
                    <input type="text" name="nama" value={inputNama} onChange={handleChangeNama} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Mata Kuliah : </b></label>
                    <input type="text" name="matakuliah" value={inputMatakul} onChange={handleChangeMatakul} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nilai : </b></label>
                    <input type="number" name="nilai" value={inputNilai} onChange={handleChangeNilai} required />
                </div>
                <div className="tombol-input">
                    <input className="tombol-submit2" type="submit" value="submit" />
                </div>
            </form>
        </>
    )
}

export default FormMahasiswa
