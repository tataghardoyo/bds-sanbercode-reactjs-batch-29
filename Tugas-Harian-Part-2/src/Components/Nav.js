import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { SwitchThemeContext } from '../Context/SwitchTheme'
import '../public/css/navigasi.css'
import { Switch } from 'antd'

const Navbar = () => {
    const { tema, setTema } = useContext(SwitchThemeContext)
    function onChange(checked) {
        if(checked){
            setTema("dark")
        }else{
            setTema("light")
        }
       
    }
    return (
        <nav className={tema}>
            <ul className="list">
                <li><Link className="link-url" to="/">Tugas 9</Link></li>
                <li><Link className="link-url" to="/tugas10">Tugas 10</Link></li>
                <li><Link className="link-url" to="/tugas11">Tugas 11</Link></li>
                <li><Link className="link-url" to="/tugas12">Tugas 12</Link></li>
                <li><Link className="link-url" to="/tugas13">Tugas 13</Link></li>
                <li><Link className="link-url" to="/tugas14">Tugas 14</Link></li>
                <li><Link className="link-url" to="/tugas15">Tugas 15</Link></li>
                <li style={{position:"absolute", right:"10px"}}><Switch defaultChecked onChange={onChange} /></li>
            </ul>
            
        </nav>
    )
}

export default Navbar
