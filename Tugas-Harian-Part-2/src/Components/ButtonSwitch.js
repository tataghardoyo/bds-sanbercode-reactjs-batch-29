import React, { useContext } from 'react'
import { SwitchThemeContext } from '../Context/SwitchTheme'
import '../public/css/tomboltema.css'

const TombolSwitch = () => {
    const { tema, setTema } = useContext(SwitchThemeContext)

    const handleWarna = () => {
        setTema(
            tema === "dark" ? "light" : "dark"
        )
    }
    return (
        <div className="pembungkus-warna">
            <button className="tombol-warna" onClick={handleWarna}>Ganti Warna Tema</button>
        </div>
    )
}

export default TombolSwitch
