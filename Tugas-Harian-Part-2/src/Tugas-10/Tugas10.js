import React from 'react'
import { useState, useEffect } from 'react'
const Tugas10 = () => {
    const [time, setTime] = useState(new Date())
    const [counter, setCounter] = React.useState(10);
   
    useEffect(() => {
        let TimeId = setInterval(() => setTime(new Date()), 1000)
        counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
   
        return () => {
            clearInterval(TimeId)
        } 
                      
    }, [counter])

    return (
        <>
            {counter > 0 &&
                <div className="pembungkus-waktu">
                    <h1>Now At - {time.toLocaleTimeString()}</h1>
                    <h3 className="waktu">Countdown : {counter}</h3>
                </div>
            }
        </>
    )
}

export default Tugas10
