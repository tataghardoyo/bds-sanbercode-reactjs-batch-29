import React, { useContext, useEffect } from "react"
import { useParams } from "react-router-dom"
import { MahasiswaContext } from "../Context/MahasiswaContext"

const StudentScoreForm = () => {
    let { id } = useParams()
    const { getDataById, inputNama, inputMatakul, inputNilai, handleChangeNama, handleChangeMatakul, handleChangeNilai, handleSubmit2 } = useContext(MahasiswaContext)
    
    useEffect(() => {
        if (id !== undefined) {
            getDataById(id)
        }
        
    },[])

    return (
        <div className="pembungkus-buah">
            <h1 className="tengah">Form Nilai Mahasiswa</h1>
            <form onSubmit={handleSubmit2} className="pembungkus-input">
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nama : </b></label>
                    <input type="text" name="nama" value={inputNama} onChange={handleChangeNama} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Mata Kuliah : </b></label>
                    <input type="text" name="matakuliah" value={inputMatakul} onChange={handleChangeMatakul} required />
                </div>
                <div className="pembungkus-form">
                    <label className="label-input"><b>Nilai : </b></label>
                    <input type="number" name="nilai" value={inputNilai} onChange={handleChangeNilai} required />
                </div>
                <div className="tombol-input">
                    <input className="tombol-submit2" type="submit" value="submit" />
                </div>
            </form>
        </div>
    )
}

export default StudentScoreForm
