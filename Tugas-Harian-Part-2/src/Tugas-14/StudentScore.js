import React from 'react'
import StudentScoreList from './StudentScoreList'

const StudentScore = () => {
    return (
        <div className="main-bungkus">
            <StudentScoreList />
        </div>
    )
}

export default StudentScore
