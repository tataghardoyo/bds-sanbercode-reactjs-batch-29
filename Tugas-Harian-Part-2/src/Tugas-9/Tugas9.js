import React from 'react'
import logo from '../public/img/logo.png';
import Checklis from '../Checklis';
const tugas9 = () => {
    return (
        <div className="pembungkus">
            <div className="tengah">
                <img src={logo} alt="gambar" />
            </div>
            <p className="judul">THINGS TO DO</p>
            <p className="sub-judul">During bootcamp in sanbercode</p>
            <hr className="garis" />
            <form>
                <Checklis labele="Belajar GIT & CLI" />
                <Checklis labele="Belajar HTML & CSS" />
                <Checklis labele="Belajar Javascript" />
                <Checklis labele="Belajar ReactJS Dasar" />
                <Checklis labele="Belajar ReactJS Advance" />
                <input className="tombol" type="submit" value="SEND" />
            </form>
        </div>
    )
}

export default tugas9
