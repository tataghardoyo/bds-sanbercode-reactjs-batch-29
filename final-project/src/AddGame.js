import React, { useState, useContext } from 'react'
import axios from "axios"
import { useHistory } from "react-router"
import Cookies from "js-cookie"
import { GameContext } from './Context/GameContext'

const AddGame = () => {
    let history = useHistory()
    const { game, setGame } = useContext(GameContext)

    const [inputGame, setInputGame] = useState({
        name: "", genre: "", singlePlayer: false, multiplayer: false,
        platform: "", release: "", image_url: ""
    })
    const handleChangeGame = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInputGame({ ...inputGame, [name]: value })
    }
    const handleChangeCek = (event) => {
        let value = event.target.checked
        let name = event.target.name

        setInputGame({ ...inputGame, [name]: value })
    }
    const handleTambahGame = (event) => {
        event.preventDefault()
        if (inputGame.release >= 2000 && inputGame.release <= 2021) {
            axios.post(`https://backendexample.sanbersy.com/api/data-game`, {
                name: inputGame.name, genre: inputGame.genre, singlePlayer: inputGame.singlePlayer,
                multiplayer: inputGame.multiplayer, platform: inputGame.platform, release: inputGame.release,
                image_url: inputGame.image_url
            }, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
                .then(res => {
                    let data = res.data
                    setGame([...game, {
                        id: data.id, name: data.name, genre: data.genre, singlePlayer: data.singlePlayer,
                        multiplayer: data.multiplayer, platform: data.platform, release: data.release,
                        image_url: data.image_url
                    }])
                    alert('Data berhasil ditambah!')
                    setInputGame({
                        id: null, name: "", genre: "", singlePlayer: false, multiplayer: false,
                        platform: "", release: "", image_url: ""
                    })
                    history.push("/datagame")
                })
        }else{
            alert('releasi 2000 - 2021')
        }
    }
    return (
        <div className="container pt-5 pb-5 bungkus-cpwd">
            <div className="pt-5 pb-5">
                <form onSubmit={handleTambahGame} className="border p-4">
                    <h2>Tambah Game</h2>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control" placeholder="Masukkan Nama" name="name" onChange={handleChangeGame} value={inputGame.name} required />
                    </div>
                    <div className="form-group">
                        <label>Genre</label>
                        <input type="text" className="form-control" placeholder="Masukkan Genre" name="genre" onChange={handleChangeGame} value={inputGame.genre} required />
                    </div>
                    <div className="form-group">
                        <label style={{ display: 'inline-block', width: '100px' }}>Single Player : </label>
                        <input type="checkbox" name="singlePlayer" onChange={handleChangeCek} checked={inputGame.singlePlayer} />
                    </div>
                    <div className="form-group">
                        <label style={{ display: 'inline-block', width: '100px' }}>Multi Player : </label>
                        <input type="checkbox" name="multiplayer" onChange={handleChangeCek} checked={inputGame.multiplayer} />
                    </div>
                    <div className="form-group">
                        <label>Platform</label>
                        <input type="text" className="form-control" placeholder="Masukkan Platform" name="platform" onChange={handleChangeGame} value={inputGame.platform} required />
                    </div>
                    <div className="form-group">
                        <label>Release</label>
                        <input type="text" className="form-control" placeholder="Masukkan Release" name="release" onChange={handleChangeGame} value={inputGame.release} required />
                    </div>
                    <div className="form-group">
                        <label>Image URL</label>
                        <input type="text" className="form-control" placeholder="Masukkan Image URL" name="image_url" onChange={handleChangeGame} value={inputGame.image_url} required />
                    </div>
                    <button type="submit" className="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    )
}

export default AddGame
