import React, { useState, useContext, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import axios from "axios"
import { MovieContext } from './Context/MovieContext'
import Cookies from "js-cookie"

const EditMovie = () => {
    let history = useHistory()
    const { film, setFilm } = useContext(MovieContext)
    let { idMovie } = useParams()
    const [editFilm, setEditFilm] = useState(
        {
            id: null, title: "", description: "", year: 0, duration: 0,
            genre: "", rating: 0, review: "", image_url: ""
        }
    )
    const handleChangeFilm = (event) => {
        let nilai = event.target.value
        let nama = event.target.name

        setEditFilm({ ...editFilm, [nama]: nilai })
    }
    useEffect(() => {
        if (idMovie !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`)
                .then(res => {
                    let data = res.data
                    setEditFilm({
                        id: data.id, title: data.title, description: data.description, year: data.year,
                        duration: data.duration, genre: data.genre, rating: data.rating, review: data.review,
                        image_url: data.image_url
                    })
                })
                .catch(err => alert(err.message))
        }

    },[])

    const handleEditFilm = (event) => {
        event.preventDefault()
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`, {
            title: editFilm.title, description: editFilm.description, year: editFilm.year,
            duration: editFilm.duration, genre: editFilm.genre, rating: editFilm.rating, review: editFilm.review,
            image_url: editFilm.image_url
        }, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
            .then((res) => {
                let data = res.data
                let saring = film.filter(item => item.id !== data.id).map(nil => nil)

                setFilm([...saring, {
                    id: data.id, title: data.title, description: data.description, year: data.year,
                    duration: data.duration, genre: data.genre, rating: data.rating, review: data.review,
                    image_url: data.image_url
                }])
                alert('Data berhasil diedit!')
                setEditFilm({
                    id: null, title: "", description: "", year: 0, duration: 0,
                    genre: "", rating: 0, review: "", image_url: ""
                })
                history.push("/datamovie")
            })

    }
    return (
        <div className="container pt-5 pb-5 bungkus-cpwd">
            <div className="pt-5 pb-5">
                <form onSubmit={handleEditFilm} className="border p-4">
                    <h2>Edit Movie</h2>
                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control" name="description" onChange={handleChangeFilm} value={editFilm.description} required />
                    </div>
                    <div className="form-group">
                        <label>Title</label>
                        <input type="text" className="form-control" placeholder="Masukkan Judul" name="title" value={editFilm.title} onChange={handleChangeFilm} required />
                    </div>
                    <div className="form-group">
                        <label>Year</label>
                        <input type="number" className="form-control" placeholder="Masukkan Tahun" name="year" onChange={handleChangeFilm} value={editFilm.year} required />
                    </div>
                    <div className="form-group">
                        <label>Duration</label>
                        <input type="number" className="form-control" placeholder="Masukkan Durasi" name="duration" onChange={handleChangeFilm} value={editFilm.duration} required />
                    </div>
                    <div className="form-group">
                        <label>Genre</label>
                        <input type="text" className="form-control" placeholder="Masukkan Genre" name="genre" onChange={handleChangeFilm} value={editFilm.genre} required />
                    </div>
                    <div className="form-group">
                        <label>Rating</label>
                        <input type="number" className="form-control" placeholder="Masukkan Rating" name="rating" onChange={handleChangeFilm} value={editFilm.rating} required />
                    </div>
                    <div className="form-group">
                        <label>Review</label>
                        <input type="text" className="form-control" placeholder="Masukkan Review" name="review" onChange={handleChangeFilm} value={editFilm.review} required />
                    </div>
                    <div className="form-group">
                        <label>Image URL</label>
                        <input type="text" className="form-control" placeholder="Masukkan Image URL" name="image_url" onChange={handleChangeFilm} value={editFilm.image_url} required />
                    </div>
                    <input type="submit" className="btn btn-primary" value="Edit" />
                </form>
            </div>
        </div>
    )
}

export default EditMovie
