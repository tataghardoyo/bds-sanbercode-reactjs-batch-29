import React, { useContext, useState } from 'react'
import { Button, Table } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import Sidebar from './Components/Sidebar'
import { Link } from 'react-router-dom'
import { GameContext } from './Context/GameContext'
const GameList = () => {
    const { game, handleEditGame, handleDeleteGame } = useContext(GameContext)

    const [hasilFill, sethasilFill] = useState(null)
    const [search, setSearch] = useState('')
    const handleChange = (e) => {
        setSearch(e.target.value)

    }
    const handleSearch = async (e) => {
        e.preventDefault()
        let hasilSearch = game.filter((item) => {
            return item.name === search

        })
        let apa = hasilSearch.map(it => it)
        sethasilFill(apa)
    }
 
    const filterData = data => formatter => data.map((item) => (
        {
            text: formatter(item.genre),
            value: formatter(item.genre)
        }
    )
    )

    const splitName = index => dataItem => dataItem.split(" ")[index]

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            filters: [...filterData(game)(splitName(0))],
            onFilter: (value, record) => record.genre.indexOf(value) === 0,
        },
        {
            title: 'Single Player',
            dataIndex: 'singlePlayer',
            key: 'singlePlayer',
            render: (res) => (
                <>
                    {
                        res ? ("yes") : ("no")
                    }
                </>
            )
        },
        {
            title: 'Multi Player',
            dataIndex: 'multiplayer',
            key: 'multiplayer',
            render: (res) => (
                <>
                    {
                        res ? ("yes") : ("no")
                    }
                </>
            )
        },
        {
            title: 'Platform',
            dataIndex: 'platform',
            key: 'platform',

        },
        {
            title: 'Release',
            dataIndex: 'release',
            key: 'release',
            sorter: {
                compare: (a, b) => a.release - b.release,
                multiple: 3,
            },
        },
        {
            title: 'Image URL',
            dataIndex: 'image_url',
            key: 'image_url',
        },
        {
            title: 'Aksi',
            key: 'action',
            render: (res, index) => (
                <>
                    <Button className="m-1" value={res.id} onClick={handleEditGame} ><EditOutlined /></Button>
                    <Button className="m-1" type="danger" value={res.id} onClick={handleDeleteGame}><DeleteOutlined /></Button>
                </>
            )

        }
    ];

    const data = hasilFill === null ? game : hasilFill

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }
    return (
        <div className="pl-4 pt-5 pr-4 pb-5">
            <div className="row pt-5 pb-5">
                <Sidebar />
                <div className="col-sm-10 bg-warning">
                    <h1 className="mt-2">Game List</h1>
                    <form onSubmit={handleSearch} style={{ position: 'absolute', right: '15px' }}>
                        <input type="text" value={search} onChange={handleChange} placeholder="masukkan judul" />
                        <input type="submit" value="Cari" />
                    </form>
                    <Link to="/creategame"><Button type="primary">Tambah</Button></Link>
                    <Table columns={columns} dataSource={data} onChange={onChange} scroll={{ x: 400 }} />
                </div>
            </div>
        </div>
    )
}

export default GameList
