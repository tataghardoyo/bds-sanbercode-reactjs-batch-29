import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { MovieContext } from './Context/MovieContext'

const Halamandetail = () => {
    let { idMovie } = useParams()
    const {detailFilm, getDataById } = useContext(MovieContext)

    useEffect(() => {
        if (idMovie !== undefined) {
            getDataById(idMovie)
        }

    })

    return (
        <div className="container p-5">
            <div className="pt-5 pb-5">
                <form className="border p-4">
                    <h2>Detail Film</h2>
                    <img src={detailFilm.image_url} alt="gambare" width="500" height="500" className="img-fluid mb-4 rounded"/>
                    <div className="form-outline mb-4">
                        <label className="form-label">Title</label>
                        <input type="text" className="form-control" value={detailFilm.title} disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Year</label>
                        <input type="text" className="form-control" value={detailFilm.year} d disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Duration</label>
                        <input type="text" className="form-control" value={detailFilm.duration} d disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Genre</label>
                        <input type="text" className="form-control" value={detailFilm.genre} d disabled />
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label">Rating</label>
                        <input type="text" className="form-control" value={detailFilm.rating} d disabled />
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label">Review</label>
                        <input type="text" className="form-control" value={detailFilm.review} d disabled />
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label">Description</label>
                        <textarea class="form-control" rows="4" value={detailFilm.description} disabled></textarea>
                    </div>


                    <button type="submit" className="btn btn-primary btn-block mb-4">Kembali</button>
                </form>
            </div>
        </div>
    )
}

export default Halamandetail
