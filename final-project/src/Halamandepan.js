import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import { GameContext } from './Context/GameContext'
import { MovieContext } from './Context/MovieContext'
const Halamandepan = () => {
    const { film } = useContext(MovieContext)
    const { game } = useContext(GameContext)

    const [hd, setHd] = useState(true)

    const ganti = () => {
        setHd(true)
    }
    const ganti2 = () => {
        setHd(false)
    }
    return (
        <div className="container pt-5 pb-5">
            <div className="btn-group btn-group-toggle pt-5">
                <button className="btn btn-secondary" onClick={ganti}>
                    Movie
                </button>
                <button className="btn btn-secondary" onClick={ganti2}>
                    Game
                </button>
            </div>
            {
                hd ? (
                    <div className="row pt-5">

                        {film.map((item, idx) => (
                            <div className="col-sm mb-4" key={idx}>
                                <div className="card mx-auto" style={{ width: '18rem' }}>
                                    <img src={item.image_url} className="card-img-top" alt="gambar card" height="300" />
                                    <div className="card-body">
                                        <h5 className="card-title">{item.title}</h5>
                                        <h6 className="card-title">{"(" + item.year + ")"}</h6>
                                        <p className="card-text">{item.description.substring(0, 30) + "..."}</p>
                                        <Link to={`/detail/${item.id}`} className="btn btn-primary">Detail</Link>
                                    </div>
                                </div>
                            </div>
                        ))

                        }
                    </div>
                ) : (
                   <div className="row pt-5">

                        {game.map((item, idex) => (
                            <div className="col-sm mb-4" key={idex}>
                                <div className="card mx-auto" style={{ width: '18rem' }}>
                                    <img src={item.image_url} className="card-img-top" alt="gambar card" height="300" />
                                    <div className="card-body">
                                        <h5 className="card-title">{item.name}</h5>
                                        <h6 className="card-title">{"(" + item.genre + ")"}</h6>
                                        <p className="card-text">{item.platform}</p>
                                        <Link to={`/detailgame/${item.id}`} className="btn btn-primary">Detail</Link>
                                    </div>
                                </div>
                            </div>
                        ))

                        }
                    </div>
                )
            }
        </div >
    )
}

export default Halamandepan

