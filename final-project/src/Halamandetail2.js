import React, { useContext, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { GameContext } from './Context/GameContext'

const Halamandetail2 = () => {
    let { idGame } = useParams()
    const {detailGame, getDataById2 } = useContext(GameContext)

    useEffect(() => {
        if (idGame !== undefined) {
            getDataById2(idGame)
        }

    })
    return (
        <div className="container p-5">
            <div className="pt-5 pb-5">
                <form className="border p-4">
                    <h2>Detail Film</h2>
                    <img src={detailGame.image_url} alt="gambare" width="500" height="500" className="img-fluid mb-4 rounded"/>
                    <div className="form-outline mb-4">
                        <label className="form-label">Name</label>
                        <input type="text" className="form-control" value={detailGame.name} disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Genre</label>
                        <input type="text" className="form-control" value={detailGame.genre} d disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Single Player</label>
                        <input type="text" className="form-control" value={detailGame.singlePlayer} d disabled />
                    </div>

                    <div className="form-outline mb-4">
                        <label className="form-label">Multi Player</label>
                        <input type="text" className="form-control" value={detailGame.multiplayer} d disabled />
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label">Platform</label>
                        <input type="text" className="form-control" value={detailGame.platform} d disabled />
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label">Release</label>
                        <input type="text" className="form-control" value={detailGame.release} d disabled />
                    </div>
                 
                    <button type="submit" className="btn btn-primary btn-block mb-4">Kembali</button>
                </form>
            </div>
        </div>
    )
}

export default Halamandetail2
