import React, { useContext, useState } from 'react'
import { Button, Table } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import Sidebar from './Components/Sidebar'
import { Link } from 'react-router-dom'
import { MovieContext } from './Context/MovieContext'

const MovieList = () => {
    const { film, handleEditMovie, handleDeleteMovie } = useContext(MovieContext)
    const [hasilFill, sethasilFill] = useState(null)
    const [search, setSearch] = useState('')
    const handleChange = (e) => {
        setSearch(e.target.value)

    }
    const handleSearch = (e) => {
        e.preventDefault()
        let hasilSearch = film.filter((item) => {
            return item.title === search

        })
        let apa = hasilSearch.map(it => it)
        sethasilFill(apa)
    }
    const filterData = data => formatter => data.map((item) => (
        {
            text: formatter(item.genre),
            value: formatter(item.genre)
        }
    )
    )

    const splitName = index => dataItem => dataItem.split(" ")[index]

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Year',
            dataIndex: 'year',
            key: 'year',
            sorter: {
                compare: (a, b) => a.year - b.year,
                multiple: 2,
            },
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            key: 'duration',
            sorter: {
                compare: (a, b) => a.duration - b.duration,
                multiple: 1,
            },
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            filters: [...filterData(film)(splitName(0))],
            onFilter: (value, record) => record.genre.indexOf(value) === 0,
        },
        {
            title: 'Rating',
            dataIndex: 'rating',
            key: 'rating',
            sorter: {
                compare: (a, b) => a.rating - b.rating,
                multiple: 1,
            },
        },
        {
            title: 'Review',
            dataIndex: 'review',
            key: 'review',
        },
        {
            title: 'Image URL',
            dataIndex: 'image_url',
            key: 'image_url',
        },
        {
            title: 'Aksi',
            key: 'action',
            render: (res, index) => (
                <>
                    <Button className="m-1" value={res.id} onClick={handleEditMovie} ><EditOutlined /></Button>
                    <Button className="m-1" type="danger" value={res.id} onClick={handleDeleteMovie} ><DeleteOutlined /></Button>
                </>
            )

        }
    ];

    const data = hasilFill === null ? film : hasilFill

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    return (
        <div className="pl-4 pt-5 pb-5 pr-4">
            <div className="row pb-5 pt-5">
                <Sidebar />
                <div className="col-sm-10 bg-warning">
                    <h1 className="mt-2">Movie List</h1>
                    <form onSubmit={handleSearch} style={{ position: 'absolute', right: '15px' }}>
                        <input type="text" value={search} onChange={handleChange} placeholder="masukkan judul" />
                        <input type="submit" value="Cari" />
                    </form>
                    <Link to="/createmovie"><Button type="primary">Tambah</Button></Link>
                    <Table columns={columns} dataSource={data} onChange={onChange} scroll={{ x: 400 }} />
                </div>
            </div>
        </div>
    )
}

export default MovieList
