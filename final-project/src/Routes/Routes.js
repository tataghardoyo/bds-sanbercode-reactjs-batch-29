import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Navbar from '../Components/Navbar'
import { GameProvider } from '../Context/GameContext'
import { MovieProvider } from '../Context/MovieContext'
import Halamandepan from '../Halamandepan'
import Login from '../Authentication/Login'
import Register from '../Authentication/Register'
import MovieList from '../MovieList'
import GameList from '../GameList'
import Footer from '../Components/Footer'
import { UserProvider } from '../Context/UserContext'
import Gantipwd from '../Authentication/Gantipwd'
import Halamandetail from '../Halamandetail'
import Halamandetail2 from '../Halamandetail2'
import AddMovie from '../AddMovie'
import AddGame from '../AddGame'
import EditMovie from '../EditMovie'
import EditGame from '../EditGame'
import Cookies from "js-cookie"

const Routes = () => {
    const LoginRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect to="/" />
        } else {
            return <Route {...props} />
        }
    }
    const PublicRoute = ({ ...props }) => {
        if (Cookies.get('token') === undefined) {
            return <Redirect to="/" />
        } else {
            return <Route {...props} />
        }
    }
    return (
        <>
            <Router>
                <UserProvider>
                    <MovieProvider>
                        <GameProvider>
                            <Navbar />
                            <Switch>
                                <Route path="/" exact>
                                    <Halamandepan />
                                </Route>
                                <Route path="/detail/:idMovie" exact>
                                    <Halamandetail />
                                </Route>
                                <Route path="/detailgame/:idGame" exact>
                                    <Halamandetail2 />
                                </Route>

                                <LoginRoute exact path="/login" component={Login} />
                                <LoginRoute exact path="/register" component={Register} />
                                <PublicRoute exact path="/datamovie" component={MovieList} />
                                <PublicRoute exact path="/createmovie" component={AddMovie} />
                                <PublicRoute exact path="/editmovie/:idMovie" component={EditMovie} />
                                <PublicRoute exact path="/datagame" component={GameList} />
                                <PublicRoute exact path="/creategame" component={AddGame} />
                                <PublicRoute exact path="/editgame/:idGame" component={EditGame} />
                                <PublicRoute exact path="/gantipsw" component={Gantipwd} />
                                
                            </Switch>
                            <Footer />
                        </GameProvider>
                    </MovieProvider>
                </UserProvider>
            </Router>
        </>
    )
}

export default Routes
