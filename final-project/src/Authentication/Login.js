import React, { useContext } from 'react'
import { Form, Input, Button } from 'antd'
import { UserContext } from '../Context/UserContext'
import axios from "axios"
import Cookies from "js-cookie"
import { useHistory } from "react-router"

const Login = () => {
    let history = useHistory()
    const { setLoginStatus } = useContext(UserContext)

    const onFinish = (values) => {
        axios.post("https://backendexample.sanbersy.com/api/user-login", {
            email: values.email,
            password: values.password
        }).then(
            (res) => {
                var user = res.data.user
                var token = res.data.token
                Cookies.set('user', user.name, { expires: 1 })
                Cookies.set('email', user.email, { expires: 1 })
                Cookies.set('token', token, { expires: 1 })
                history.push('/')
                setLoginStatus(true)
            }
        ).catch((err) => {
            alert(err)
        })
        //alert(values.email+" "+values.password)
    }

    return (

        <div className="container bungkus-login">
            <div className="border row ml-2 mr-2 mb-5">
                <div className="container-fluid mt-4 ml-3 col-sm">
                    <img className="img-fluid rounded" src="https://placeimg.com/350/212/any" alt="gambar login" />
                </div>
                <div className="w-100 ml-3 col-sm">
                    <h2 className="mt-3 mb-4">Masuk Akun</h2>
                    <Form
                        name="basic"
                        labelCol={{ span: 6 }}
                        wrapperCol={{ span: 18 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        autoComplete="off"
                    >
                        <Form.Item
                            name="email"
                            rules={[{ required: true, message: 'Please input your email!' }]}
                        >
                            <Input placeholder="Masukkan email" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                        >
                            <Input.Password placeholder="Masukkan Password" />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Login
                            </Button>
                        </Form.Item>

                    </Form>
                </div>
            </div>
        </div>

    )
}

export default Login

