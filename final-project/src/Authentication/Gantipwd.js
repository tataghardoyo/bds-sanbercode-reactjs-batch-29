import React, {useState} from 'react'
import axios from "axios"
import Cookies from "js-cookie"
import { useHistory } from "react-router"

const Gantipwd = () => {
    let history = useHistory()
    const [inputPwd, setInputPwd] = useState({
        current_password: "", new_password: "", new_confirm_password: ""
    })

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInputPwd({ ...inputPwd, [name]: value })
    }

    const handlePassword = (event) => {
        if (inputPwd.new_password === inputPwd.new_confirm_password) {
            event.preventDefault()
            axios.post(`https://backendexample.sanbersy.com/api/change-password`, {
                current_password: inputPwd.current_password, new_password: inputPwd.new_password, new_confirm_password: inputPwd.new_confirm_password,
            }, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
                .then(res => {
                
                    alert('Data berhasil ditambah!')
                    setInputPwd({
                        id: null, current_password: "", new_password: "", new_confirm_password: "",
                    })
                    history.push("/")
                })
        } else {
            alert("password tidak sama")
        }
    }
    return (
        <div className="container pt-5 bungkus-cpwd">
            <div className="pt-5">
                <form onSubmit={handlePassword} className="border p-4">
                    <h2>Ganti Password</h2>
                    <div className="form-group">
                        <label>Current Password</label>
                        <input type="password" className="form-control" name="current_password" onChange={handleChange} value={inputPwd.current_password} placeholder="Enter email" />
                    </div>
                    <div className="form-group">
                        <label>New Password</label>
                        <input type="password" className="form-control" name="new_password" onChange={handleChange} value={inputPwd.new_password} placeholder="Password" />
                    </div>
                    <div className="form-group">
                        <label>New Confirm Password</label>
                        <input type="password" className="form-control" name="new_confirm_password" onChange={handleChange} value={inputPwd.new_confirm_password} placeholder="Password" />
                    </div>
                    <button type="submit" className="btn btn-primary">Konfirmasi</button>
                </form>
            </div>
        </div>
    )
}

export default Gantipwd
