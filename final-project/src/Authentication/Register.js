import React from 'react'
import { Form, Input, Button } from 'antd'
import axios from "axios"
import { useHistory } from "react-router"

const Register = () => {
    let history = useHistory()

    const onFinish = (values) => {
        axios.post("https://backendexample.sanbersy.com/api/register", {
            name: values.name,
            email: values.email,
            password: values.password
        }).then(
            () => {
                history.push('/login')
            }
        ).catch((err) => {
            alert(err)
        })
        // alert(values.name+" "+values.email+" "+values.password)
    }

    return (
        <div className="container bungkus-login">
            <div className="border row ml-2 mr-2 mb-5">
                <div className="container-fluid mt-4 ml-3 col-sm">
                    <img className="img-fluid rounded" src="https://placeimg.com/350/212/any" alt="gambar regis"/>
                </div>
                <div className="w-100 ml-3 col-sm">
                    <h2 className="mt-3 mb-4">Daftar Akun</h2>
                    <Form
                        name="basic"
                        labelCol={{ span: 6 }}
                        wrapperCol={{ span: 18 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        autoComplete="off"
                    >
                        <Form.Item
                            name="name"
                            rules={[{ required: true, message: 'Please input your name!' }]}
                        >
                            <Input placeholder="Masukkan Nama" />
                        </Form.Item>

                        <Form.Item
                            name="email"
                            rules={[{ required: true, message: 'Please input your email!' }]}
                        >
                            <Input placeholder="Masukkan Email" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: 'Please input your password!' }]}
                        >
                            <Input.Password placeholder="Masukkan Password" />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Register
                            </Button>
                        </Form.Item>

                    </Form>
                </div>
            </div>
        </div>
    )
}

export default Register
