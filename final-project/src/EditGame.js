import React, { useState, useContext, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import axios from "axios"
import Cookies from "js-cookie"
import { GameContext } from './Context/GameContext'

const EditGame = () => {
    let history = useHistory()
    let { idGame } = useParams()
    const { game, setGame } = useContext(GameContext)

    const [editGame, setEditGame] = useState({
        id: null, name: "", genre: "", singlePlayer: false, multiplayer: false,
        platform: "", release: "", image_url: ""
    })
    const handleChangeGame = (event) => {
        let nilai = event.target.value
        let nama = event.target.name

        setEditGame({ ...editGame, [nama]: nilai })
    }
    const handleChangeCek = (event) => {
        let nilai = event.target.checked
        let nama = event.target.name

        setEditGame({ ...editGame, [nama]: nilai })
    }
    useEffect(() => {
        if (idGame !== undefined) {
            axios.get(`https://backendexample.sanbersy.com/api/data-game/${idGame}`)
                .then(res => {
                    let data = res.data
                    setEditGame({
                        id: data.id, name: data.name, genre: data.genre, singlePlayer: data.singlePlayer,
                        multiplayer: data.multiplayer, platform: data.platform, release: data.release,
                        image_url: data.image_url
                    })
                })
        }

    }, [])
    const handleEditGame = (event) => {
        event.preventDefault()
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {
            name: editGame.name, genre: editGame.genre, singlePlayer: editGame.singlePlayer,
            multiplayer: editGame.multiplayer, platform: editGame.platform, release: editGame.release,
            image_url: editGame.image_url
        }, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
            .then((res) => {
                let data = res.data
                let saring = game.filter(item => item.id !== data.id).map(nil => nil)
                setGame([...saring, {
                    id: data.id, name: data.name, genre: data.genre, singlePlayer: data.singlePlayer,
                    multiplayer: data.multiplayer, platform: data.platform, release: data.release,
                    image_url: data.image_url
                }])
                alert('Data berhasil diedit!')
                setEditGame({
                    id: null, name: "", genre: "", singlePlayer: false, multiplayer: false,
                    platform: "", release: "", image_url: ""
                })
                history.push("/datagame")
            })
    }
    return (
        <div className="container pt-5 pb-5 bungkus-cpwd">
            <div className="pt-5 pb-5">
                <form onSubmit={handleEditGame} className="border p-4">
                    <h2>Edit Game</h2>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control" placeholder="Masukkan Nama" name="name" onChange={handleChangeGame} value={editGame.name} required />
                    </div>
                    <div className="form-group">
                        <label>Genre</label>
                        <input type="text" className="form-control" placeholder="Masukkan Genre" name="genre" onChange={handleChangeGame} value={editGame.genre} required />
                    </div>
                    <div className="form-group">
                        <label style={{ display: 'inline-block', width: '100px' }}>Single Player : </label>
                        <input type="checkbox" name="singlePlayer" checked={editGame.singlePlayer} onChange={handleChangeCek} />
                    </div>
                    <div className="form-group">
                        <label style={{ display: 'inline-block', width: '100px' }}>Multi Player : </label>
                        <input type="checkbox" name="multiplayer" checked={editGame.multiplayer} onChange={handleChangeCek} />
                    </div>
                    <div className="form-group">
                        <label>Platform</label>
                        <input type="text" className="form-control" placeholder="Masukkan Platform" name="platform" onChange={handleChangeGame} value={editGame.platform} required />
                    </div>
                    <div className="form-group">
                        <label>Release</label>
                        <input type="text" className="form-control" placeholder="Masukkan Release" name="release" onChange={handleChangeGame} value={editGame.release} required />
                    </div>
                    <div className="form-group">
                        <label>Image URL</label>
                        <input type="text" className="form-control" placeholder="Masukkan Image URL" name="image_url" onChange={handleChangeGame} value={editGame.image_url} required />
                    </div>
                    <input type="submit" className="btn btn-primary" value="Edit" />
                </form>
            </div>
        </div>
    )
}

export default EditGame
