import React, { createContext, useState, useEffect } from 'react'
import axios from "axios"
import { useHistory } from 'react-router-dom'
import Cookies from "js-cookie"
export const MovieContext = createContext()

export const MovieProvider = (props) => {
    const history = useHistory()
    const [film, setFilm] = useState([])
    const [detailFilm, setDetailFilm] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            setFilm(result.data.map(x => {
                return {
                    id: x.id, title: x.title, description: x.description, year: x.year,
                    duration: x.duration, genre: x.genre, rating: x.rating, review: x.review, image_url: x.image_url,
                }
            }))
        }
        fetchData()
    }, [])

    const handleEditMovie = (event) => {
        let idMovie = parseInt(event.currentTarget.value)
        history.push(`/editmovie/${idMovie}`)
    }

    const getDataById = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
            .then(res => {
                let data = res.data
                setDetailFilm(data)
            })
    }
    const handleDeleteMovie = (event) => {
        let idMovie = parseInt(event.currentTarget.value)

        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie}`, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
            .then((res) => {
                let data = res.data
                setFilm(film.filter(item => item.id !== idMovie))
                alert('Data Terhapus')
            })
    }
    return (
        <MovieContext.Provider value={{
            film,
            setFilm,
            handleEditMovie,
            getDataById,
            detailFilm,
            setDetailFilm,
            handleDeleteMovie
        }}>
            {props.children}
        </MovieContext.Provider>
    )
}