import React, { createContext, useState, useEffect } from 'react'
import axios from "axios"
import { useHistory } from 'react-router-dom'
import Cookies from "js-cookie"
export const GameContext = createContext()

export const GameProvider = (props) => {
    const history = useHistory()
    const [game, setGame] = useState([])
    const [detailGame, setDetailGame] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            setGame(result.data.map(x => {
                return {
                    id: x.id, name: x.name, genre: x.genre, singlePlayer: x.singlePlayer,
                    multiplayer: x.multiplayer, platform: x.platform, release: x.release, image_url: x.image_url,
                }
            }))
        }
        fetchData()
    }, [])

    const handleEditGame = (event) => {
        let idGame = parseInt(event.currentTarget.value)
        history.push(`/editgame/${idGame}`)
    }
    const getDataById2 = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
            .then(res => {
                let data = res.data
                setDetailGame(data)
            })
    }
    const handleDeleteGame = (event) => {
        let idGame = parseInt(event.currentTarget.value)

        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
            .then((res) => {
                let data = res.data
                setGame(game.filter(el => { return el.id !== idGame }))
                alert('Data Terhapus')
            })
    }
    return (
        <GameContext.Provider value={{
            game,
            setGame,
            handleEditGame,
            getDataById2,
            detailGame,
            setDetailGame,
            handleDeleteGame
        }}>
            {props.children}
        </GameContext.Provider>
    )
}
