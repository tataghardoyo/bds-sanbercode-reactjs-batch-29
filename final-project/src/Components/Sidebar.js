import React from 'react'
import { Link } from 'react-router-dom'

const Sidebar = () => {
    return (
        <aside className="col-sm-2 px-0">
            <ul className="list-group pr-0">
                <Link to="/datamovie"><li className="list-group-item">Data Movie</li></Link>
                <Link to="/datagame"><li className="list-group-item">Data Game</li></Link>
            </ul>
        </aside>
    )
}

export default Sidebar
