import React from 'react'

const Footer = () => {
    return (
        <footer className="fixed-bottom pt-3">
            Copyright&copy; 2021 - Hardaya
        </footer>
    )
}

export default Footer
