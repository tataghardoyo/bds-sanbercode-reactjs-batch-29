import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import Cookies from "js-cookie"
import { useHistory } from "react-router"
import { UserContext } from '../Context/UserContext'

const Navbar = () => {
    const { setLoginStatus } = useContext(UserContext)
    let history = useHistory()

    const handleLogout = () => {
        setLoginStatus(false)
        Cookies.remove('user')
        Cookies.remove('email')
        Cookies.remove('token')
        history.push('/')
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light atas fixed-top">
            <Link className="navbar-brand" to="/">Beranda</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    {Cookies.get('token') === undefined && (
                        <>
                            <Link className="nav-item nav-link" to="/login">Login</Link>
                            <Link className="nav-item nav-link" to="/register">Register</Link>
                        </>
                    )
                    }
                    {Cookies.get('token') !== undefined && (
                        <Link className="nav-item nav-link" to="/datamovie">Data</Link>
                    )
                    }

                </div>
                {
                    Cookies.get('token') !== undefined && (
                        <div className="navbar-nav ml-auto">
                            <li className="nav-item dropdown">
                                <span className="nav-link dropdown-toggle panah" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {Cookies.get('email')}
                                </span>
                                <div className="dropdown-menu dropdown-menu-right p-1" aria-labelledby="navbarDropdown">
                                    <Link className="nav-item nav-link" to="/gantipsw">Ganti Password</Link>
                                    <span className="nav-item nav-link panah" onClick={handleLogout}>Logout</span>
                                </div>
                            </li>
                        </div>
                    )
                }
            </div>
        </nav>
    )
}

export default Navbar
