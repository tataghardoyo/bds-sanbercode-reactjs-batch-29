import React, { useState, useContext } from 'react'
import { MovieContext } from './Context/MovieContext'
import axios from "axios"
import { useHistory } from "react-router"
import Cookies from "js-cookie"

function AddMovie() {
    let history = useHistory()
    const { film, setFilm } = useContext(MovieContext)
    const [input, setInput] = useState({
        title: "", description: "", year: 0, duration: 0,
        genre: "", rating: 0, review: "", image_url: ""
    })
    const handleChangeFilm = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({ ...input, [name]: value })
    }
    const handleTambahFilm = (event) => {
        event.preventDefault()
        if (input.rating >= 0 && input.rating <= 10 && input.year >= 1980 && input.year <= 2021) {
            axios.post(`https://backendexample.sanbersy.com/api/data-movie`, {
                title: input.title, description: input.description, year: input.year,
                duration: input.duration, genre: input.genre, rating: input.rating, review: input.review,
                image_url: input.image_url
            }, { headers: { "Authorization": "Bearer " + Cookies.get('token') } })
                .then(res => {
                    let data = res.data
                    setFilm([...film, {
                        id: data.id, title: data.title, description: data.description, year: data.year,
                        duration: data.duration, genre: data.genre, rating: data.rating, review: data.review,
                        image_url: data.image_url
                    }])
                    alert('Data berhasil ditambah!')
                    setInput({
                        id: null, title: "", description: "", year: 0, duration: 0,
                        genre: "", rating: 0, review: "", image_url: ""
                    })
                    history.push("/datamovie")
                })
        } else {
            alert('rating 0 - 10 dan year 1980 - 2021')
        }

    }
    return (
        <div className="container pt-5 pb-5 bungkus-cpwd">
            <div className="pt-5 pb-5">
                <form onSubmit={handleTambahFilm} className="border p-4">
                    <h2>Tambah Movie</h2>
                    <div className="form-group">
                        <label>Title</label>
                        <input type="text" name="title" className="form-control" placeholder="Masukkan Judul" onChange={handleChangeFilm} value={input.title} required />
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control" name="description" onChange={handleChangeFilm} value={input.description} required />
                    </div>
                    <div className="form-group">
                        <label>Year</label>
                        <input type="number" className="form-control" name="year" placeholder="Masukkan Tahun" onChange={handleChangeFilm} value={input.year} required />
                    </div>
                    <div className="form-group">
                        <label>Duration</label>
                        <input type="number" className="form-control" name="duration" placeholder="Masukkan Durasi" onChange={handleChangeFilm} value={input.duration} required />
                    </div>
                    <div className="form-group">
                        <label>Genre</label>
                        <input type="text" className="form-control" name="genre" placeholder="Masukkan Genre" onChange={handleChangeFilm} value={input.genre} required />
                    </div>
                    <div className="form-group">
                        <label>Rating</label>
                        <input type="number" className="form-control" name="rating" placeholder="Masukkan Rating" onChange={handleChangeFilm} value={input.rating} required />
                    </div>
                    <div className="form-group">
                        <label>Review</label>
                        <input type="text" className="form-control" name="review" placeholder="Masukkan Review" onChange={handleChangeFilm} value={input.review} required />
                    </div>
                    <div className="form-group">
                        <label>Image URL</label>
                        <input type="text" className="form-control" name="image_url" placeholder="Masukkan Image URL" onChange={handleChangeFilm} value={input.image_url} required />
                    </div>
                    <button type="submit" className="btn btn-primary">Tambah</button>
                </form>
            </div>
        </div>
    )
}

export default AddMovie
